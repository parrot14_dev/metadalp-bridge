use std::{borrow::BorrowMut, io::{self, Read}};

use ipc::IPC;
use serde::{Deserialize, Serialize};

mod ipc;

#[derive(Serialize, Deserialize)]
struct DownloadMessage {
    title: String,
    domain: String,
    url: String,
}

fn main() {
    let mut bytes = io::stdin().bytes();
    let ipc = IPC::new();

    loop {
        let byte_array = bytes.borrow_mut()
            .take(4)
            .map(|elm| elm.unwrap())
            .enumerate()
            .fold([0u8;4], |mut arr, (i, val)|{
                arr[i]=val;
                arr
            });
        let len = u32::from_le_bytes(byte_array) as usize;
        let payload: Vec<u8> = bytes.borrow_mut()
            .take(len)
            .map(|elm| elm.unwrap())
            .collect();

        let DownloadMessage{title, domain, url} = serde_json::from_slice(&payload).unwrap();

        ipc.send(title, domain, url)
    }
}
