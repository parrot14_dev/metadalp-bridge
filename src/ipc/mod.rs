use zbus::blocking::Connection;

use self::linux::MetadalpDownloaderProxyBlocking;

mod linux;

pub struct IPC<'a>{
    _connection: Connection,
    proxy: linux::MetadalpDownloaderProxyBlocking<'a>
}

impl IPC<'static>{
    pub fn new() -> IPC<'static> {
        let connection = Connection::session().unwrap();
        let proxy = MetadalpDownloaderProxyBlocking::new(&connection).unwrap();

        IPC { _connection: connection, proxy }
    }

    pub fn send(&self, placeholder_title: String, domain: String, url: String) {
        let _reply = self.proxy.Download(placeholder_title, domain, url);
    }
}