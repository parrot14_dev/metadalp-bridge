use zbus::proxy;

#[proxy(
    interface = "mx.connorchickenway.MetadalpDownloader",
    default_service = "mx.connorchickenway.MetadalpDownloader",
    default_path = "/MetadalpDownloader"
)]
trait MetadalpDownloader {
    async fn Download(&self, 
        placeholder_title: String ,
        domain: String,
        url: String) -> zbus::Result<()>;
}